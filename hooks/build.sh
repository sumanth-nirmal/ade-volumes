img_name=$1
img_flavour=$2
# fully qualified name for image
img_fqn=""
[[ -z "$img_flavour" ]] && img_fqn=$img_name || img_fqn=$img_name-$img_flavour

echo "Building image "$img_fqn

# change to the directory where the Dockerfile exists 
cd $img_name

# build docker image
docker build \
   --network host \
   --label ade_image_commit_sha="$CI_COMMIT_SHA" \
   --label ade_image_commit_tag="$CI_COMMIT_TAG" \
   -t "$img_fqn":latest .

echo "Pushing image "$img_fqn
registry_img=$CI_REGISTRY_IMAGE/$img_fqn

echo "Tagging image to "$registry_img
echo $CI_REGISTRY
echo $CI_REGISTRY_IMAGE
echo $CI_PROJECT_NAMESPACE
echo $CI_REGISTRY/$CI_PROJECT_NAMESPACE:latest
# upload docker image
docker tag "$img_fqn":latest $registry_img:latest
docker push $registry_img:latest

